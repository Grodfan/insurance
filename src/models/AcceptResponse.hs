{-# LANGUAGE DeriveGeneric #-}

module AcceptResponse where

import Data.UUID (UUID)
import GHC.Generics (Generic)
import Data.Aeson (ToJSON)

newtype AcceptResponse = AcceptResponse { insurancePolicyId :: UUID } deriving (Generic)

instance ToJSON AcceptResponse
