{-# LANGUAGE DeriveGeneric #-}

module GenerateResponse where

import Data.UUID (UUID)
import GHC.Generics (Generic)
import Data.Aeson (ToJSON)

newtype GenerateResponse = GenerateResponse { quoteId :: UUID } deriving (Generic)

instance ToJSON GenerateResponse