{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}

module Quote where

import qualified Data.Text as T
import GHC.Generics (Generic)
import Data.Aeson (ToJSON, FromJSON)
import Data.Time ( Day )

data Quote = Quote
   { applicantName  :: T.Text
   , startDate      :: Day
   , endDate        :: Day
   , itemPrice      :: Double
   } deriving (Generic, Show, ToJSON, FromJSON)
