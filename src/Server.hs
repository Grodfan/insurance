{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeOperators              #-}

module Server where

import Database.Beam ( MonadIO(liftIO))
import Database.Beam.Postgres
    ( close,
      connect,
      defaultConnectInfo,
      ConnectInfo(connectHost, connectDatabase, connectUser,
                  connectPassword),
      Connection )
import Data.UUID ( UUID )
import Data.Pool(createPool, Pool)
import Servant.Server ()
import Servant
    ( Proxy(..),
      serve,
      err404,
      err400,
      Server,
      Handler,
      ServerError(errBody),
      throwError,
      type (:<|>)(..),
      Capture,
      JSON,
      ReqBody,
      type (:>),
      Get,
      Post,
      NoContent (NoContent) )
import Network.Wai.Handler.Warp (run)
import Quote ( Quote )
import AcceptResponse ( AcceptResponse(AcceptResponse) )
import PendingQuoteTable ( PendingQuote, PendingQuoteT (_accepted, _rejected, _quoteId))
import InsurancePolicyTable
    ( InsurancePolicy, InsurancePolicyT(_insurancePolicyId) )
import qualified PendingQuoteQueries as P
import qualified InsurancePolicyQueries as I
import GenerateResponse (GenerateResponse (GenerateResponse))

type PendingQuotesAPI = "accept" :> Capture "pendingQuoteId" UUID  :> Post '[JSON] AcceptResponse
                   :<|> "discard" :> Capture "pendingQuoteId" UUID :> Post '[JSON] NoContent
                   :<|> "generate" :> ReqBody '[JSON] Quote :> Post '[JSON] GenerateResponse

type InsurancePolicyAPI = Capture "insurancePolicyId" UUID :> Get '[JSON] InsurancePolicy

type API = "pendingquotes" :> PendingQuotesAPI
      :<|> "insurancepolicy" :> InsurancePolicyAPI

pendingQuotesServer :: Pool Connection -> Server PendingQuotesAPI
pendingQuotesServer pool = accept
                      :<|> discard
                      :<|> generate
   where
         accept :: UUID -> Handler AcceptResponse
         accept pId = do
            x <- liftIO $ P.find pool pId
            case x of
               Nothing -> throwError $ err404 { errBody = "Quote not found." }
               (Just c) -> if _accepted c then 
                              throwError $ err400 { errBody = "Already accepted." }
                           else 
                              do
                              _ <- liftIO $ P.accept pool pId
                              p  <- liftIO $ I.create pool c
                              return $ AcceptResponse $ _insurancePolicyId p

         discard :: UUID -> Handler NoContent
         discard pId = do
            x <- liftIO $ P.find pool pId
            case x of
               Nothing -> throwError $ err404 { errBody = "Quote not found." }
               (Just c) -> if _rejected c then 
                              throwError $ err400 { errBody = "Already rejected." }
                           else
                              do
                              liftIO $ P.discard pool pId
                              return NoContent

         generate :: Quote -> Handler GenerateResponse
         generate quote = do
            x <- liftIO $ P.create pool quote
            return $ GenerateResponse $ _quoteId x

insurancePolicyServer :: Pool Connection -> Server InsurancePolicyAPI
insurancePolicyServer pool = getInsurancePolicy
   where
         getInsurancePolicy :: UUID -> Handler InsurancePolicy
         getInsurancePolicy pId = do
            x <- liftIO $ I.find pool pId
            case x of
               Nothing -> throwError $ err404 { errBody = "Insurance policy not found." }
               (Just c) -> do return c

server :: Pool Connection -> Server API
server pool = pendingQuotesServer pool :<|> insurancePolicyServer pool

connectionString :: ConnectInfo
connectionString = defaultConnectInfo
     { connectHost = "abul.db.elephantsql.com"
     , connectDatabase = "otdpaztp"
     , connectUser = "otdpaztp"
     , connectPassword = "lu_ZRR8pR5ODMBFgTq_agSpgby5Y_vva"
     }

newConnection :: IO Connection
newConnection = do
   connect connectionString

runServer :: IO ()
runServer = do
   putStrLn "Server running - localhost:8080"
   pool <- createPool newConnection close 1 40 10
   run 8080 $ serve (Proxy :: Proxy API) (server pool)
