module InsurancePolicyQueries where
    
import Data.Pool (Pool, withResource)
import Database.Beam.Postgres (Connection, runBeamPostgres)
import Data.UUID (UUID)
import InsurancePolicyTable
    ( InsurancePolicy,
      InsurancePolicyT(InsurancePolicy, _insurancePolicyId) )
import PendingQuoteTable
    ( PendingQuote,
      PendingQuoteT(_applicantName, _insuranceStartDate,
                    _insuranceEndDate, _insuredItemPrice, _insurancePrice) )
import Database.Beam.Backend.SQL.BeamExtensions (MonadBeamInsertReturning(runInsertReturningList))
import Database.Beam
    ( insert,
      insertExpressions,
      runSelectReturningOne,
      select,
      all_,
      default_,
      filter_,
      SqlValable(val_),
      SqlEq((==.)) )
import DbSettings ( InsuranceDb(_insurancePolicy), insuranceDb )

create:: Pool Connection -> PendingQuote -> IO InsurancePolicy
create pool pq = withResource pool retrive
   where retrive conn = do
                     [x] <- runBeamPostgres conn $
                              runInsertReturningList $
                              insert (_insurancePolicy insuranceDb) $
                              insertExpressions
                                 [ InsurancePolicy
                                    default_
                                    (val_ (_applicantName pq))
                                    (val_ (_insuranceStartDate pq))
                                    (val_ (_insuranceEndDate pq))
                                    (val_ (_insuredItemPrice pq))
                                    (val_ (_insurancePrice pq))
                                 ]
                     return x

find :: Pool Connection -> UUID -> IO (Maybe InsurancePolicy)
find pool x = withResource pool retrive
   where retrive conn = runBeamPostgres conn $
                        runSelectReturningOne $ select $
                        filter_ (\c -> _insurancePolicyId c ==. val_ x) $
                        all_ (_insurancePolicy insuranceDb)
