module PendingQuoteQueries
   ( accept
   , create
   , discard
   , find
   ) where

import Database.Beam.Postgres ( runBeamPostgres, Connection )
import Data.Pool ( withResource, Pool )
import Data.UUID (UUID)
import PendingQuoteTable
    ( PendingQuote,
      PendingQuoteT(PendingQuote, _accepted, _rejected, _quoteId) )
import Quote
    ( Quote(applicantName, startDate, endDate, itemPrice) )
import Database.Beam
    ( insert,
      insertExpressions,
      runSelectReturningOne,
      runUpdate,
      select,
      update,
      all_,
      default_,
      filter_,
      (<-.),
      SqlValable(val_),
      SqlEq((==.)) ) 
import DbSettings ( InsuranceDb(_pendingQuote), insuranceDb )
import Database.Beam.Backend.SQL.BeamExtensions (MonadBeamInsertReturning(runInsertReturningList))

accept :: Pool Connection -> UUID -> IO ()
accept pool x = withResource pool retrive
   where retrive conn = runBeamPostgres conn $
                        runUpdate $ update (_pendingQuote insuranceDb)
                           (\c -> _accepted c <-. val_ True)
                           (\c -> _quoteId c ==. val_ x)

create :: Pool Connection -> Quote -> IO PendingQuote
create pool q = withResource pool retrive
   where calculateInsurancePrice = 0.2 * itemPrice q
         retrive conn = do
                  [x] <- runBeamPostgres conn $
                           runInsertReturningList $
                           insert (_pendingQuote insuranceDb) $
                           insertExpressions
                              [ PendingQuote
                                 default_
                                 (val_ (applicantName q))
                                 (val_ (startDate q))
                                 (val_ (endDate q))
                                 (val_ (itemPrice q))
                                 (val_ calculateInsurancePrice)
                                 (val_ False)
                                 (val_ False)
                              ]
                  return x

discard :: Pool Connection -> UUID -> IO ()
discard pool x = withResource pool retrive
   where retrive conn = runBeamPostgres conn $
                        runUpdate $ update (_pendingQuote insuranceDb)
                           (\c -> _rejected c <-. val_ True)
                           (\c -> _quoteId c ==. val_ x)

find :: Pool Connection -> UUID -> IO (Maybe PendingQuote)
find pool x = withResource pool retrive
   where retrive conn = runBeamPostgres conn $
                        runSelectReturningOne $ select $
                        filter_ (\c -> _quoteId c ==. val_ x) $
                        all_ (_pendingQuote insuranceDb)
