{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}

module DbSettings where

import PendingQuoteTable
    ( PendingQuoteT(_quoteId, _applicantName, _insuranceStartDate,
                    _insuranceEndDate, _insuredItemPrice, _insurancePrice, _rejected,
                    _accepted) )
import InsurancePolicyTable
    ( InsurancePolicyT(_insurancePolicyId, _name, _startDate, _endDate,
                       _itemPrice, _price) )
import GHC.Generics (Generic)
import Database.Beam
    ( dbModification,
      defaultDbSettings,
      modifyTableFields,
      setEntityName,
      tableModification,
      withDbModification,
      Database,
      DatabaseSettings, 
      TableEntity
    )

data InsuranceDb f = InsuranceDb
    { _pendingQuote    :: f (TableEntity PendingQuoteT)
    , _insurancePolicy :: f (TableEntity InsurancePolicyT)
    } deriving (Generic, Database be)

insuranceDb :: DatabaseSettings be InsuranceDb
insuranceDb = defaultDbSettings `withDbModification`
                  dbModification
                  { _pendingQuote =
                        setEntityName "pending_quote" <>
                           modifyTableFields
                              tableModification
                              { _quoteId = "quote_id"
                              , _applicantName = "applicant_name"
                              , _insuranceStartDate = "insurance_start_date"
                              , _insuranceEndDate = "insurance_end_date"
                              , _insuredItemPrice = "insured_item_price"
                              , _insurancePrice = "insurance_price"
                              , _rejected = "rejected"
                              , _accepted = "accepted"
                              }
                  , _insurancePolicy =
                        setEntityName "insurance_policy" <>
                           modifyTableFields
                              tableModification
                              { _insurancePolicyId = "policy_id"
                              , _name = "applicant_name"
                              , _startDate = "insurance_start_date"
                              , _endDate = "insurance_end_date"
                              , _itemPrice = "insured_item_price"
                              , _price = "insurance_price"
                              }
                  }
