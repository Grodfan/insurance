{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}

module PendingQuoteTable where

import Data.UUID (UUID)
import qualified Data.Text as T
import Database.Beam
    ( Identity, Beamable, Columnar, Table(..) )
import GHC.Generics (Generic)
import Data.Aeson (ToJSON)
import Data.Time (Day)

data PendingQuoteT f = PendingQuote
   { _quoteId            :: Columnar f UUID
   , _applicantName      :: Columnar f T.Text
   , _insuranceStartDate :: Columnar f Day
   , _insuranceEndDate   :: Columnar f Day
   , _insuredItemPrice   :: Columnar f Double
   , _insurancePrice     :: Columnar f Double
   , _rejected           :: Columnar f Bool
   , _accepted           :: Columnar f Bool
   } deriving (Generic, Beamable)

type PendingQuote = PendingQuoteT Identity
type QuoteId = PrimaryKey PendingQuoteT Identity

instance ToJSON PendingQuote
deriving instance Show PendingQuote
deriving instance Eq PendingQuote
deriving instance Show QuoteId
deriving instance Eq QuoteId

instance Table PendingQuoteT where
    data PrimaryKey PendingQuoteT f = QuoteId (Columnar f UUID) deriving (Generic, Beamable)
    primaryKey = QuoteId . _quoteId
