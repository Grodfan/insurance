{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}

module InsurancePolicyTable where

import Data.UUID (UUID)
import qualified Data.Text as T
import GHC.Generics (Generic)
import Data.Aeson (ToJSON)
import Database.Beam
    ( Identity, Beamable, Columnar, Table(..) )
import Data.Time (Day)
    
data InsurancePolicyT f = InsurancePolicy
   { _insurancePolicyId :: Columnar f UUID
   , _name               :: Columnar f T.Text
   , _startDate          :: Columnar f Day
   , _endDate            :: Columnar f Day
   , _itemPrice          :: Columnar f Double
   , _price              :: Columnar f Double
   } deriving (Generic, Beamable)

instance ToJSON InsurancePolicy
type InsurancePolicy = InsurancePolicyT Identity
type InsurancePolicyId = PrimaryKey InsurancePolicyT Identity

deriving instance Show InsurancePolicy
deriving instance Eq InsurancePolicy
deriving instance Show InsurancePolicyId
deriving instance Eq InsurancePolicyId

instance Table InsurancePolicyT where
   data PrimaryKey InsurancePolicyT f = InsurancePolicyId (Columnar f UUID) deriving (Generic, Beamable)
   primaryKey = InsurancePolicyId . _insurancePolicyId
