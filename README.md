# Insurance

**Install**
1) Clone repo
2) Open terminal. 
3) Go to root folder of the repo.
4) run "stack build && stack exec insurance-exe" 
5) If you have postman installed. You can import the collection in the postman folder.

Optional\
The database is temporary running on ElephantSQL.\
If you want to run on your local machine: 

1) Install PostgreSQL. 
2) Create a database and a user
3) Run the create.sql script, located in sql folder.
4) Update the connectionString in Server.hs

**Test**
1) Open terminal.
2) Go to root folder of the repo and run "stack test".

Generate a new quote and you will recevie a new quoteId.
You can Accept or Discard the quote. If you Accept you will recevie a new insurancePolicyId, and use the
insurance policy API to get the insurance policy.

**API**

**Quote**

**Generate (POST) - {{URL}}/pendingquotes/generate/**

Example:\
Body - JSON\
{\
&nbsp;&nbsp;"applicantName": "Kalle Svensson",\
&nbsp;&nbsp;"startDate": "2012-11-12",\
&nbsp;&nbsp;"endDate" : "2034-11-12",\
&nbsp;&nbsp;"itemPrice" : 100\
}

Repsonse - 200 OK\
{\
&nbsp;&nbsp;"quoteId": "4baba3b4-a1de-4d09-9228-356fdb07998b"\
}

**Accept (POST) - {{URL}}/pendingquotes/accept/{{QuoteId}}**

Example:\
localhost:8080/pendingquotes/accept/4baba3b4-a1de-4d09-9228-356fdb07998b

Response - 200 OK\
{\
&nbsp;&nbsp;"insurancePolicyId": "4d1aab93-0b8a-4e9f-8fcb-58a3b23cfd86"\
}

**Discard (POST) - {{URL}}/pendingquotes/discard/{{QuoteId}}**

Example:\
localhost:8080/pendingquotes/discard/2c535835-3308-4d3c-be56-9aee8e009136

Response - 200 OK

**Insurance policy**

**Get - {{URL}}/insurancepolicy/{{InsurancePolicyId}}**

Example:\
localhost:8080/insurancepolicy/4d1aab93-0b8a-4e9f-8fcb-58a3b23cfd86

Response - 200 OK\
{\
&nbsp;&nbsp;"_name": "Kalle Svensson",\
&nbsp;&nbsp;"_insurancePolicyId": "4d1aab93-0b8a-4e9f-8fcb-58a3b23cfd86",\
&nbsp;&nbsp;"_endDate": "2034-11-12",\
&nbsp;&nbsp;"_itemPrice": 100,\
&nbsp;&nbsp;"_price": 20,\
&nbsp;&nbsp;"_startDate": "2012-11-12"\
}

**TODO:**
- Add more tests.
- More refactoring.

