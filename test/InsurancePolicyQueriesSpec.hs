module InsurancePolicyQueriesSpec where

import Test.Hspec ( shouldBe, it, describe, Spec, around )
import qualified Data.Text as T
import qualified PendingQuoteQueries as P
import qualified InsurancePolicyTable as I
import qualified InsurancePolicyQueries as I
import Helper ( withDatabaseConnection, getInsurancePolicy, restoreDb )
import Quote ( Quote(Quote) )
import Data.Time (fromGregorian, getCurrentTime, UTCTime (utctDay))

spec :: Spec
spec = do
    around withDatabaseConnection $ do
        describe "InsurancePolicyQuery" $ do
            it "should be created" $ \pool -> do
                sDate <- utctDay <$> getCurrentTime
                eDate <- utctDay <$> getCurrentTime 
                let q = Quote (T.pack "Kalle") sDate eDate 100
                pendingQuote <- P.create pool q
                res <- I.create pool pendingQuote
                _ <- restoreDb pool
                I._name res `shouldBe` T.pack "Kalle"

            it "should be found" $ \pool -> do
                sDate <- utctDay <$> getCurrentTime
                eDate <- utctDay <$> getCurrentTime 
                let q = Quote (T.pack "Kalle") sDate eDate 100
                pendingQuote <- P.create pool q
                insurancePolicy <- I.create pool pendingQuote
                res <- I.find pool (I._insurancePolicyId insurancePolicy)
                _ <- restoreDb pool
                I._name (getInsurancePolicy res) `shouldBe` T.pack "Kalle"
