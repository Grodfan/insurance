{-# LANGUAGE OverloadedStrings #-}

module PendingQuoteQueriesSpec where

import PendingQuoteTable ( PendingQuoteT(_quoteId) )
import Test.Hspec ( shouldBe, it, describe, around, Spec )
import Quote (Quote(Quote))
import qualified Data.Text as T
import qualified PendingQuoteTable as P
import qualified PendingQuoteQueries as P
import Helper ( getPendingQuote, withDatabaseConnection, restoreDb )
import Data.Time (UTCTime(utctDay), getCurrentTime)

spec :: Spec
spec = do
    around withDatabaseConnection $ do
        describe "PendingQuoteQuery" $ do
            it "should be accepted" $ \pool -> do
                sDate <- utctDay <$> getCurrentTime
                eDate <- utctDay <$> getCurrentTime 
                let q = Quote (T.pack "Kalle") sDate eDate 100
                pendingQuote <- P.create pool q
                _ <- P.accept pool (_quoteId pendingQuote)
                res <- P.find pool (_quoteId pendingQuote)
                _ <- restoreDb pool
                P._accepted (getPendingQuote res) `shouldBe` True

            it "should be created" $ \pool -> do
                sDate <- utctDay <$> getCurrentTime
                eDate <- utctDay <$> getCurrentTime 
                let q = Quote (T.pack "Kalle") sDate eDate 100
                res <- P.create pool q
                _ <- restoreDb pool
                P._applicantName res `shouldBe` T.pack "Kalle"

            it "should be rejected" $ \pool -> do
                sDate <- utctDay <$> getCurrentTime
                eDate <- utctDay <$> getCurrentTime 
                let q = Quote (T.pack "Kalle") sDate eDate 100
                pendingQuote <- P.create pool q
                _ <- P.discard pool (_quoteId pendingQuote)
                res <- P.find pool (_quoteId pendingQuote)
                _ <- restoreDb pool
                P._rejected (getPendingQuote res) `shouldBe` True

            it "should be found" $ \pool -> do
                sDate <- utctDay <$> getCurrentTime
                eDate <- utctDay <$> getCurrentTime 
                let q = Quote (T.pack "Kalle") sDate eDate 100
                pendingQuote <- P.create pool q
                res <- P.find pool (_quoteId pendingQuote)
                _ <- restoreDb pool
                P._applicantName (getPendingQuote res) `shouldBe` T.pack "Kalle"

            it "should calculate item insurance price" $ \pool -> do
                sDate <- utctDay <$> getCurrentTime
                eDate <- utctDay <$> getCurrentTime 
                let itemPrice = 100
                let q = Quote (T.pack "Kalle") sDate eDate itemPrice
                let insurancePrice = 0.2 * itemPrice
                res <- P.create pool q
                _ <- restoreDb pool
                P._insurancePrice res `shouldBe` insurancePrice
