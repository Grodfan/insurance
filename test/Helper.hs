{-# LANGUAGE OverloadedStrings #-}

module Helper
    ( getPendingQuote
    , getInsurancePolicy
    , withDatabaseConnection
    , newConnection
    , restoreDb
    ) where

import Data.UUID (UUID)
import PendingQuoteTable (PendingQuote)
import InsurancePolicyTable (InsurancePolicy)
import Control.Exception ( bracket )
import Database.PostgreSQL.Simple
    ( execute_,
      ConnectInfo(connectHost, connectDatabase, connectUser,
                  connectPassword),
      defaultConnectInfo,
      connect,
      close,
      Connection )
import Data.Int ( Int64 )
import Data.Pool (Pool, withResource, createPool, destroyAllResources)

getPendingQuote :: Maybe PendingQuote -> PendingQuote
getPendingQuote Nothing = error "not found"
getPendingQuote (Just x) = x

getInsurancePolicy :: Maybe InsurancePolicy -> InsurancePolicy
getInsurancePolicy Nothing = error "not found"
getInsurancePolicy (Just x) = x

connectionString :: ConnectInfo
connectionString = defaultConnectInfo
     { connectHost = "abul.db.elephantsql.com"
     , connectDatabase = "alitgnez"
     , connectUser = "alitgnez"
     , connectPassword = "egoyZu7nRyz_k4qu9eWIdL3orGQKKv3C"
     }

newConnection :: IO Connection
newConnection = do
   connect connectionString

withDatabaseConnection :: (Pool Connection -> IO ())-> IO ()
withDatabaseConnection = bracket openConnection closeConnection

openConnection :: IO (Pool Connection)
openConnection = do
    createPool newConnection close 1 40 10

closeConnection :: Pool Connection -> IO ()
closeConnection = destroyAllResources

restoreDb :: Pool Connection -> IO Int64
restoreDb pool = do
    _ <- truncatePendingQuoteTable pool
    truncateInsurancePolicyTable pool

truncatePendingQuoteTable :: Pool Connection -> IO Int64
truncatePendingQuoteTable pool = withResource pool clear
    where clear conn = execute_ conn "TRUNCATE TABLE pending_quote;"

truncateInsurancePolicyTable :: Pool Connection -> IO Int64
truncateInsurancePolicyTable pool = withResource pool clear
    where clear conn = execute_ conn "TRUNCATE TABLE insurance_policy;"
