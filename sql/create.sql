DROP TABLE IF EXISTS pending_quote;
DROP TABLE IF EXISTS insurance_policy;

CREATE TABLE pending_quote (
    quote_id UUID DEFAULT gen_random_uuid(),
    applicant_name VARCHAR NOT NULL,
    insurance_start_date DATE NOT NULL,
    insurance_end_date DATE NOT NULL,
    insured_item_price DOUBLE PRECISION NOT NULL,
    insurance_price DOUBLE PRECISION NOT NULL,
    rejected BOOLEAN NOT NULL,
    accepted BOOLEAN NOT NULL
);

CREATE TABLE insurance_policy (
    policy_id UUID DEFAULT gen_random_uuid(),
    applicant_name VARCHAR NOT NULL,
    insurance_start_date DATE NOT NULL,
    insurance_end_date DATE NOT NULL,
    insured_item_price DOUBLE PRECISION  NOT NULL,
    insurance_price DOUBLE PRECISION  NOT NULL
);