INSERT INTO pending_quote (
    applicant_name, 
    insurance_start_date, 
    insurance_end_date, 
    insured_item_price, 
    insurance_price, 
    rejected, 
    accepted ) 
VALUES (
    'KalleKula', 
    '2021-12-12', 
    '2022-12-12', 
    120.00, 
    24.00, 
    false,
    false );

SELECT * FROM pending_quote;